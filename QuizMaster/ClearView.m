//
//  ClearView.m
//  QuizMaster
//
//  Created by Mark Aufflick on 16/12/2014.
//  Copyright (c) 2014 The High Technology Bureau. All rights reserved.
//

#import "ClearView.h"

@implementation ClearView

- (void)drawRect:(NSRect)dirtyRect
{
    [super drawRect:dirtyRect];
    
    [[NSColor clearColor] set];
    NSRectFill(dirtyRect);
}

@end
