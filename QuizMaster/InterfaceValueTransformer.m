//
//  InterfaceValueTransformer.m
//  QuizMaster
//
//  Created by Mark Aufflick on 17/12/2014.
//  Copyright (c) 2014 The High Technology Bureau. All rights reserved.
//

#import "InterfaceValueTransformer.h"
#import <MACollectionUtilities/MACollectionUtilities.h>

@implementation InterfaceValueTransformer

+ (BOOL)allowsReverseTransformation
{
    return NO;
}

- (id)transformedValue:(NSArray *)value
{
    return [value ma_map:^id(NSDictionary * i) {
        return [NSString stringWithFormat:@"%@ : %@", i[@"interface"], i[@"address"]];
    }];
}

@end
