//
//  Question.h
//  QuizMaster
//
//  Created by Mark Aufflick on 16/12/2014.
//  Copyright (c) 2014 The High Technology Bureau. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Team;

@interface Question : NSManagedObject

@property (nonatomic, retain) NSNumber * number;
@property (nonatomic, retain) Team *team;
@property (nonatomic, retain) NSNumber * value;

@end
