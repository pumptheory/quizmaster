//
//  Question.m
//  QuizMaster
//
//  Created by Mark Aufflick on 16/12/2014.
//  Copyright (c) 2014 The High Technology Bureau. All rights reserved.
//

#import "Question.h"
#import "Team.h"


@implementation Question

@dynamic number;
@dynamic team;
@dynamic value;

@end
