//
//  QuestionSumTransformer.m
//  QuizMaster
//
//  Created by Mark Aufflick on 17/12/2014.
//  Copyright (c) 2014 The High Technology Bureau. All rights reserved.
//

#import "QuestionSumTransformer.h"

@implementation QuestionSumTransformer

+ (BOOL)allowsReverseTransformation
{
    return NO;
}

- (id)transformedValue:(NSSet *)questions
{
    return [questions valueForKeyPath:@"@sum.value"];
}

@end
