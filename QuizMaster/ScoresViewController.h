//
//  ScoresViewController.h
//  QuizMaster
//
//  Created by Mark Aufflick on 16/12/2014.
//  Copyright (c) 2014 The High Technology Bureau. All rights reserved.
//

#import "TeamListViewController.h"

@interface ScoresViewController : TeamListViewController

@end
