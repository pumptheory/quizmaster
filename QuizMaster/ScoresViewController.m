//
//  ScoresViewController.m
//  QuizMaster
//
//  Created by Mark Aufflick on 16/12/2014.
//  Copyright (c) 2014 The High Technology Bureau. All rights reserved.
//

#import "ScoresViewController.h"
#import "Team.h"
#import "WebServerVC.h"
#import "TeamCellView.h"

@interface ScoresViewController () <NSTableViewDelegate>

@property (nonatomic, strong) NSTimer * clearTimer;

@end

@implementation ScoresViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.arrayController.sortDescriptors = @[
                                             [NSSortDescriptor sortDescriptorWithKey:@"wonQuestions"
                                                                           ascending:NO
                                                                          comparator:^NSComparisonResult(NSSet * obj1, NSSet * obj2) {
                                                                              NSNumber * c1 = [obj1 valueForKeyPath:@"@sum.value"];
                                                                              NSNumber * c2 = [obj2 valueForKeyPath:@"@sum.value"];
                                                                              return [c1 compare:c2];
                                                                          }],
                                             ];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(buzz:) name:WebServerVCTeamDidBuzzNotification object:nil];
    
}

- (NSIndexSet *)tableView:(NSTableView *)tableView selectionIndexesForProposedSelection:(NSIndexSet *)proposedSelectionIndexes
{
    return [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 0)];
}

- (void)buzz:(NSNotification *)notif
{
    [self.clearTimer invalidate];
    
    Team * team = notif.userInfo[@"team"];
    NSInteger i = 0;
    for (Team * t in self.arrayController.arrangedObjects)
    {
        TeamCellView * teamCell = [self.tableView viewAtColumn:0 row:i makeIfNecessary:NO];
        teamCell.selected = [t.objectID isEqual:team.objectID];
        i++;
    }
    
    self.clearTimer = [NSTimer scheduledTimerWithTimeInterval:120 target:self selector:@selector(clearSelectionTimer:) userInfo:nil repeats:NO];
}

- (void)clearSelectionTimer:(NSTimer *)timer
{
    [self buzz:nil];
}

@end
