//
//  ScoresWindow.m
//  QuizMaster
//
//  Created by Mark Aufflick on 17/12/2014.
//  Copyright (c) 2014 The High Technology Bureau. All rights reserved.
//

#import "ScoresWindow.h"

@implementation ScoresWindow

- (instancetype)initWithContentRect:(NSRect)contentRect styleMask:(NSUInteger)aStyle backing:(NSBackingStoreType)bufferingType defer:(BOOL)flag
{
    if (nil == (self = [super initWithContentRect:contentRect styleMask:aStyle backing:bufferingType defer:flag]))
        return nil;
    
    [self setOpaque:NO];
    self.backgroundColor = [NSColor clearColor];
    self.ignoresMouseEvents = YES;
    
    return self;
}

- (BOOL)canBecomeKeyWindow
{
    return NO;
}

@end
