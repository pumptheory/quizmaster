//
//  Team.h
//  QuizMaster
//
//  Created by Mark Aufflick on 16/12/2014.
//  Copyright (c) 2014 The High Technology Bureau. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Question;

@interface Team : NSManagedObject

@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *wonQuestions;
@property (nonatomic, retain) NSString * uuid;

- (void)score;
- (void)score:(NSInteger)delta;

@end

@interface Team (CoreDataGeneratedAccessors)

- (void)addWonQuestionsObject:(Question *)value;
- (void)removeWonQuestionsObject:(Question *)value;
- (void)addWonQuestions:(NSSet *)values;
- (void)removeWonQuestions:(NSSet *)values;

@end
