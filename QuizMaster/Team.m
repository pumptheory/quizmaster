//
//  Team.m
//  QuizMaster
//
//  Created by Mark Aufflick on 16/12/2014.
//  Copyright (c) 2014 The High Technology Bureau. All rights reserved.
//

#import "Team.h"
#import "Question.h"
#import <MagicalRecord/NSManagedObject+MagicalRecord.h>
#import <MagicalRecord/NSManagedObject+MagicalRequests.h>

@implementation Team

@dynamic created;
@dynamic name;
@dynamic wonQuestions;
@dynamic uuid;

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    self.created  = [NSDate date];
    self.uuid = [[NSUUID UUID] UUIDString];
}

- (void)score
{
    [self score:1];
}

- (void)score:(NSInteger)delta
{
    NSInteger max = 0;
    for (Question * eq in [Question MR_executeFetchRequest:[Question MR_requestAll]])
        max = MAX(max, [eq.number integerValue]);
    
    Question * q = [Question MR_createEntity];
    q.number = @(max + 1);
    q.value = @(delta);
    [self addWonQuestionsObject:q];
}

@end
