//
//  TeamCellView.h
//  QuizMaster
//
//  Created by Mark Aufflick on 18/12/2014.
//  Copyright (c) 2014 The High Technology Bureau. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface TeamCellView : NSTableCellView

@property (nonatomic) BOOL selected;

@end
