//
//  TeamCellView.m
//  QuizMaster
//
//  Created by Mark Aufflick on 18/12/2014.
//  Copyright (c) 2014 The High Technology Bureau. All rights reserved.
//

#import "TeamCellView.h"
@import QuartzCore;

@implementation TeamCellView

- (void)setSelected:(BOOL)selected
{
    _selected = selected;
    
    NSView * v = [self.subviews firstObject];
    v.layer.backgroundColor = selected ? [[NSColor yellowColor] CGColor] : [[NSColor clearColor] CGColor];
}

@end
