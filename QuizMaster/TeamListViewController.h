//
//  ViewController.h
//  QuizMaster
//
//  Created by Mark Aufflick on 16/12/2014.
//  Copyright (c) 2014 The High Technology Bureau. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface TeamListViewController : NSViewController

@property (strong) IBOutlet NSArrayController *arrayController;
@property (nonatomic, readonly) NSManagedObjectContext *moc;
@property (strong) IBOutlet NSTableView *tableView;

@end

