//
//  ViewController.m
//  QuizMaster
//
//  Created by Mark Aufflick on 16/12/2014.
//  Copyright (c) 2014 The High Technology Bureau. All rights reserved.
//

#import "TeamListViewController.h"
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+Setup.h>
#import <MagicalRecord/NSManagedObjectContext+MagicalRecord.h>
#import "Team.h"

@interface TeamListViewController ()

@property (nonatomic, strong) NSWindowController * scoresWC;
@property (nonatomic, strong) NSNumber * pointsToAdd;

@end

@implementation TeamListViewController

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    
    [MagicalRecord setupCoreDataStack];
    _moc = [NSManagedObjectContext MR_defaultContext];
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.arrayController.sortDescriptors = @[
                                             [NSSortDescriptor sortDescriptorWithKey:@"created" ascending:YES]
                                             ];
    
    self.pointsToAdd = @1;
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}
- (IBAction)addPoints:(id)sender
{
    Team * team = [self.arrayController.selectedObjects firstObject];
    [team score:[self.pointsToAdd integerValue]];
    
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
    
    if (![localContext commitEditing]) {
        NSLog(@"%@:%@ unable to commit editing before saving", [self class], NSStringFromSelector(_cmd));
    }
    
    if ([localContext hasChanges])
    {
        [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
            if (!success)
                [[NSApplication sharedApplication] presentError:error];
        }];
    }
}

- (IBAction)resetScores:(id)sender
{
    for (Team * team in self.arrayController.arrangedObjects)
        team.wonQuestions = [NSSet set];
}
- (IBAction)showScores:(id)sender
{
    NSScreen * presenterScreen = self.view.window.screen;
    NSScreen * presentationScreen = presenterScreen;
    NSArray * screens = [NSScreen screens];
    if ([screens count] > 1)
    {
        for (NSScreen * screen in screens)
            if (screen != presenterScreen)
                presentationScreen = screen;
    }
    
    self.scoresWC = [self.storyboard instantiateControllerWithIdentifier:@"ScoresWC"];
    
    [(NSPanel *)self.scoresWC.window setBecomesKeyOnlyIfNeeded:YES];
    [self.scoresWC.window setLevel:CGShieldingWindowLevel()];
    
    [self.scoresWC.window setFrame:presentationScreen.visibleFrame display:YES];
    [self.scoresWC showWindow:nil];
}

@end
