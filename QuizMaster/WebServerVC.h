//
//  WebServerVC.h
//  QuizMaster
//
//  Created by Mark Aufflick on 17/12/2014.
//  Copyright (c) 2014 The High Technology Bureau. All rights reserved.
//

#import <Cocoa/Cocoa.h>

extern NSString * const WebServerVCTeamDidBuzzNotification;

@interface WebServerVC : NSViewController

@end
