//
//  WebServerVC.m
//  QuizMaster
//
//  Created by Mark Aufflick on 17/12/2014.
//  Copyright (c) 2014 The High Technology Bureau. All rights reserved.
//

#import "WebServerVC.h"
#import <MagicalRecord/CoreData+MagicalRecord.h>
#import <GCDHttpd/GCDHttpd.h>
#import "Team.h"
#import <GRMustache/GRMustache.h>
#import <MACollectionUtilities/MACollectionUtilities.h>

NSString * const WebServerVCTeamDidBuzzNotification = @"WebServerVCTeamDidBuzzNotification";

@interface WebServerVC () <NSSpeechSynthesizerDelegate, NSSoundDelegate>
{
    GCDHttpd * httpd;
    dispatch_queue_t httpdQueue;
}

@property (nonatomic, strong) NSNumber * port;
@property (nonatomic) NSInteger selectedInterface;
@property (nonatomic, strong) NSArray * interfaces;
@property (nonatomic, strong) NSString * scoreKeeperPassword;
@property (nonatomic) BOOL serverRunning;

// this can be accessed on all threads
@property (atomic) BOOL buzzerSounding;

// these are for the main thread
@property (nonatomic, strong) NSSpeechSynthesizer * speechSynth;
@property (nonatomic, strong) Team * buzzedInTeam;
@property (nonatomic, strong) NSArray * sounds;

@end

@implementation WebServerVC

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.port = @8888;
    NSLog(@"voices: %@", [NSSpeechSynthesizer availableVoices]);
    self.speechSynth = [[NSSpeechSynthesizer alloc] initWithVoice:@"com.apple.speech.synthesis.voice.daniel"];
    self.speechSynth.delegate = self;
    
    NSMutableArray * sounds = [NSMutableArray array];
    for (int i=0; i<=6; i++)
    {
        NSSound * sound = [NSSound soundNamed:[NSString stringWithFormat:@"%d", i]];
        sound.delegate = self;
        [sounds addObject:sound];
    }
    
    self.sounds = sounds;
}

- (void)viewWillAppear
{
    [super viewWillAppear];
    self.interfaces = [GCDHttpd interfaceList];
}

- (IBAction)start:(NSButton *)sender
{
    if (httpd.httpdState == 1)
    {
        [httpd stop];
        sender.title = @"Start";
        self.serverRunning = NO;
        return;
    }
    
    if ([self.scoreKeeperPassword length] == 0)
    {
        NSAlert * alert = [NSAlert alertWithMessageText:@"You need a scorekeeper password"
                                          defaultButton:@"OK"
                                        alternateButton:nil
                                            otherButton:nil
                              informativeTextWithFormat:@"Can't trust people these days"];
        [alert beginSheetModalForWindow:self.view.window completionHandler:nil];

        return;
    }
    
    httpdQueue = dispatch_queue_create("QuizMasterHttpdQueue", DISPATCH_QUEUE_CONCURRENT);
    
    httpd = [[GCDHttpd alloc] initWithDispatchQueue:httpdQueue];
    httpd.port = [self.port integerValue];
    httpd.interface = self.interfaces[self.selectedInterface][@"interface"];
    
    [httpd addTarget:self action:@selector(rootIndex:) forMethod:@"GET" role:@"/"];
    [httpd addTarget:self action:@selector(registerIndex:) forMethod:@"GET" role:@"/register"];
    [httpd addTarget:self action:@selector(registerTeam:) forMethod:@"POST" role:@"/register"];
    [httpd addTarget:self action:@selector(scoreKeeperIndex:) forMethod:@"GET" role:@"/scorekeeper"];
    [httpd addTarget:self action:@selector(scoreKeeperForm:) forMethod:@"POST" role:@"/scorekeeper"];
    [httpd addTarget:self action:@selector(teamIndex:) forMethod:@"GET" role:@"/team/:teamUUID"];
    [httpd addTarget:self action:@selector(buzz:) forMethod:@"GET" role:@"/team/:teamUUID/buzz"];

    //[httpd serveResource:@"screen.png" forRole:@"/screen.png"];   // Resource in the main bundle
    
    [httpd start];
    if (httpd.httpdState == 1)
    {
        sender.title = @"Stop";
        self.serverRunning = YES;
    }
}

- (NSString *)rootIndex:(GCDRequest *)request
{
    NSArray * teams = [Team MR_executeFetchRequest:[Team MR_requestAllSortedBy:@"created" ascending:YES]];
    
    NSError * error;
    GRMustacheTemplate * t = [GRMustacheTemplate templateFromResource:@"index" bundle:nil error:&error];
    NSAssert(t, @"template error: %@", error);
    
    NSString * ret = [t renderObject:@{ @"teams": teams } error:&error];
    NSAssert(ret, @"template render error: %@", error);
    
    return ret;
}

- (id)scoreKeeperIndex:(GCDRequest *)request
{
    NSError * error;
    GRMustacheTemplate * t = [GRMustacheTemplate templateFromResource:@"scoreKeeperIndex" bundle:nil error:&error];
    NSAssert(t, @"template error: %@", error);
    
    NSString * ret = [t renderObject:@{} error:&error];
    NSAssert(ret, @"template render error: %@", error);
    
    return ret;
}

- (id)scoreKeeperForm:(GCDRequest *)request
{
    NSString * password = request.POST[@"password"];
    password = [password stringByReplacingOccurrencesOfString:@"+" withString:@" "];
    password = [password stringByRemovingPercentEncoding];

    if (![password isEqualTo:self.scoreKeeperPassword])
    {
        GCDResponse * r = [request responseWithStatus:401 message:@"Nice try sucker"];
        
        return r;
    }
    
    NSArray * teams = [Team MR_executeFetchRequest:[Team MR_requestAllSortedBy:@"created" ascending:YES]];
    
    NSString * uuid = request.POST[@"team"];
    
    if (uuid)
    {
        NSInteger incr = [request.POST[@"incr"] integerValue];
        NSInteger decr = [request.POST[@"decr"] integerValue];
        
        Team * team = [[teams ma_select:^BOOL(Team * t) {
            return [t.uuid isEqualTo:uuid];
        }] firstObject];
        
        NSManagedObjectContext * moc = [NSManagedObjectContext MR_contextForCurrentThread];
        
        team = [team MR_inContext:moc];
        
        if (incr)
            [[team MR_inContext:moc] score:incr];
        else if (decr)
            [[team MR_inContext:moc] score:-decr];

        [moc MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
            if (!success)
                NSLog(@"error saving moc after scorekeeping: %@", error);
        }];
    }
    
    NSError * error;
    GRMustacheTemplate * t = [GRMustacheTemplate templateFromResource:@"scoreKeeperForm" bundle:nil error:&error];
    NSAssert(t, @"template error: %@", error);
    
    NSString * ret = [t renderObject:@{ @"teams": teams, @"password" : password } error:&error];
    NSAssert(ret, @"template render error: %@", error);
    
    return ret;
}

- (NSString *)registerIndex:(id)request
{
    NSError * error;
    GRMustacheTemplate * t = [GRMustacheTemplate templateFromResource:@"register" bundle:nil error:&error];
    NSAssert(t, @"template error: %@", error);
    
    // method overload hack...
    NSDictionary * args = [request isKindOfClass:[NSString class]] ? @{ @"errors" : @[request] } : @{};
    
    NSString * ret = [t renderObject:args error:&error];
    NSAssert(ret, @"template render error: %@", error);
    
    return ret;
}

static NSString * norm(NSString * str)
{
    NSString * ret = [str lowercaseString];
    ret = [ret stringByTrimmingCharactersInSet:[NSCharacterSet punctuationCharacterSet]];
    ret = [ret stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    return ret;
}

- (id)registerTeam:(GCDRequest *)request
{
    NSString * name = request.POST[@"name"];
    name = [name stringByReplacingOccurrencesOfString:@"+" withString:@" "];
    name = [name stringByRemovingPercentEncoding];
    name = [name stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    
    __block NSString * uuid;
    
    dispatch_sync(dispatch_get_main_queue(), ^{
        NSManagedObjectContext * moc = [NSManagedObjectContext MR_defaultContext];
        
        NSArray * teams = [Team MR_executeFetchRequest:[Team MR_requestAll]];
        
        NSString * normName = norm(name);
        for (Team * i in teams)
        {
            if ([norm(i.name) isEqualToString:normName])
                return;
        }
        
        Team * team = [Team MR_createInContext:moc];
        team.name = name;
        
        [moc insertObject:team];

        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
        
        if (![localContext commitEditing]) {
            NSLog(@"%@:%@ unable to commit editing before saving", [self class], NSStringFromSelector(_cmd));
        }
        
        if ([localContext hasChanges])
        {
            [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                if (!success)
                  NSLog(@"error saving moc after team rego: %@", error);
            }];
        }

        uuid = team.uuid;
    });
    
    if (uuid)
    {
        GCDResponse * r = [request responseWithStatus:303 message:@"See Other"];
        r.headers[@"Location"] = [NSString stringWithFormat:@"/team/%@", uuid];
        
        return r;
    }
    else
    {
        return [self registerIndex:[NSString stringWithFormat:@"Someone beat you to that awesome name (%@), Sorry!", name]];
    }
}

- (id)teamIndex:(GCDRequest *)request
{
    NSString * uuid = request.pathBindings[@"teamUUID"];
    
    Team * team = [[Team MR_executeFetchRequest:[Team MR_requestAllWhere:@"uuid" isEqualTo:uuid]] firstObject];
    
    if (team == nil || team.name == nil)
    {
        GCDResponse * r = [request responseWithStatus:303 message:@"See Other"];
        r.headers[@"Location"] = @"/";
        
        return r;
    }
    
    NSError * error;
    GRMustacheTemplate * t = [GRMustacheTemplate templateFromResource:@"teamIndex" bundle:nil error:&error];
    NSAssert(t, @"template error: %@", error);
    
    NSDictionary * args = @{
                            @"name" : team.name,
                            @"uuid" : uuid,
                            };
    
    NSString * ret = [t renderObject:args error:&error];
    NSAssert(ret, @"template render error: %@", error);
    
    return ret;
}

- (id)buzz:(GCDRequest *)request
{
    if (self.buzzerSounding)
        return @"nobuzz";
    
    self.buzzerSounding = YES;
    
    NSString * uuid = request.pathBindings[@"teamUUID"];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSArray * teams = [Team MR_executeFetchRequest:[Team MR_requestAllSortedBy:@"created" ascending:YES]];

        NSInteger teamIdx = 0;
        Team * team = nil;
        for (team in teams)
        {
            if ([team.uuid isEqualToString:uuid])
                break;
            
            teamIdx++;
        }
        
        if (team == nil)
        {
            self.buzzerSounding = NO;
            return;
        }
        
        self.buzzedInTeam = team;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:WebServerVCTeamDidBuzzNotification
                                                            object:self
                                                          userInfo:@{ @"team" : team }];
        
        NSSound * sound = self.sounds[teamIdx % [self.sounds count]];
        [sound play];
    });
    
    return @"buzz";
}

- (void)speechSynthesizer:(NSSpeechSynthesizer *)sender didFinishSpeaking:(BOOL)finishedSpeaking
{
    self.buzzerSounding = NO;
}

- (void)sound:(NSSound *)sound didFinishPlaying:(BOOL)aBool
{
    if (aBool)
    {
        [self.speechSynth startSpeakingString:self.buzzedInTeam.name];
    }
    else
    {
        self.buzzerSounding = NO;
    }
}

@end
